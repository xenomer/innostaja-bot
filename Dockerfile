FROM node:16-alpine

RUN apk add --update --no-cache \
  bash \
  git
#   python3 && \
#   ln -sf python3 /usr/bin/python && \
#   python3 -m ensurepip

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

ENV DATA='/data'

CMD [ "npm", "start" ]
import { APIInteractionGuildMember, APIMessage, Snowflake } from "discord-api-types";
import { ButtonInteraction, CacheType, CommandInteraction, GuildMember, Message, TextBasedChannel } from "discord.js";
import { client, guilds, i18n } from "./main";
import { format } from 'string-kit';

export function memberHasRole(member: GuildMember | APIInteractionGuildMember, roleId: Snowflake) {
    if(Array.isArray(member.roles)) {
        return member.roles.includes(roleId);
    } else {
        return member.roles.cache.has(roleId);
    }
}

const lockdowns = new Map();
export async function handleAccess(interaction: CommandInteraction<CacheType>, id: string): Promise<boolean> {
    const guildConfig = guilds[interaction.guild.id];
    if(!guildConfig) {
        interaction.reply({
            content: ':pinched_fingers: Konfiguraatio puuttuu.',
            embeds: [],
            components: [],
        })
        return false
    } else if(!memberHasRole(interaction.member, guildConfig.adminRole) || lockdowns.get(id)) {
        await interaction.reply({
            content: 'Stuntti **seis** :police_officer:',
        });
        return false;
    } else {
        return true;
    }
}
export async function setLockdown(id: string, value: boolean) {
    lockdowns.set(id, value);
}

export function ii(str: string, ...arg: any[]) {
    return format(str, ...arg)
}

export async function awaitButtonCancellation(message: APIMessage | Message, channel: TextBasedChannel, onCancel: (interaction: ButtonInteraction) => void) {
    const collector = channel.createMessageComponentCollector({
        filter: (component) => component.message.id === message.id
            && component.componentType === 'BUTTON'
            && component.component.customId === i18n.buttons.cancel.id,
        max: 1,
        message,
        idle: 300000,
    });
    collector.on('collect', (component: ButtonInteraction) => {
        onCancel(component);
    });
    
}

export function idleStatus() {
    client.user.presence.set({
        status: 'idle',
        activities: [{
            type: 'COMPETING',
            name: '💤💤💤'
        }]
    })
}
export function activeStatus() {
    client.user.presence.set({
        status: 'online',
        activities: [{
            name: 'Innostajat',
            type: 'STREAMING',
            url: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        }]
    })
}
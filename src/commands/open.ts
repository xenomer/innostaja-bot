import { SlashCommandBuilder } from '@discordjs/builders';
import { CacheType, ColorResolvable, CommandInteraction, MessageActionRow, MessageButton, MessageEmbed } from 'discord.js';
import _ from 'lodash';
import { mainDebug, guilds, client, i18n as i } from '../main';
import { activeStatus, awaitButtonCancellation, idleStatus, ii } from '../utils';
import { promisify } from 'util';

const wait = promisify(setTimeout);
const debug = mainDebug.extend('open');

export const data = new SlashCommandBuilder()
    .setName(i.commands.open_command)
    .setDescription(i.commands.open_command_description)
    .addStringOption(option => 
        option.setName(i.commands.open_command_event)
            .setDescription(i.commands.open_command_event_description)
            .setRequired(false)
    )
export const perms = { access: true, lockdown: true, name: 'openclose' }
export async function execute(interaction: CommandInteraction<CacheType>) {
    const guildConfig = guilds[interaction.guild.id];
    const { updateChannel: updateChannelId, openRole } = guildConfig;
    const members = await interaction.guild.members.fetch()
    const isOpen = members
        .every(member => member.roles.cache.has(openRole));
    if(isOpen) {
        await interaction.reply({
            content: i.commands.open_already_open,
            ephemeral: true,
        });
        return;
    }

    const user = interaction.user;
    const event = interaction.options.getString(i.commands.open_command_event);
    const eventName = _.upperFirst(event)
    const embed = new MessageEmbed()
        .setTitle(i.commands.open_embed_title)
        .setDescription(ii(i.commands.open_embed_description, user.id))
        .setColor(i.commands.embed_progress_color as ColorResolvable)
        .setTimestamp()
    const eventField = event ? {
        name: i.commands.open_embed_event_name,
        value: ii(i.commands.open_embed_event_value, eventName),
    } : null;
    if(eventField) {
        embed.addFields(eventField);
    }
    await interaction.reply({
        embeds: [
            embed
        ],
        components: [
            new MessageActionRow()
                .addComponents(
                    new MessageButton()
                        .setCustomId(i.buttons.cancel.id)
                        .setLabel(i.buttons.cancel.label)
                        .setStyle('DANGER')
                ),
        ]
    });

    let cancelled = false;
    
    awaitButtonCancellation(
        await interaction.fetchReply(),
        interaction.channel,
        async (buttonInteraction) => {
            debug('open was cancelled by %s', buttonInteraction.user.tag);
            cancelled = true;

            embed.setTitle(i.commands.open_embed_title_cancelled_progress);
            embed.setDescription(i.commands.open_embed_description_cancelled_progress);
            embed.setColor(i.commands.embed_cancel_color as ColorResolvable);
            await interaction.editReply({
                embeds: [ embed ],
                components: [ ],
            })

            for(const member of members.values()) {
                if (member.roles.cache.has(openRole)) {
                    await member.roles.remove(openRole);
                    await wait(1000);
                }
            }

            embed.setTitle(i.commands.open_embed_title_cancelled);
            embed.setDescription(ii(i.commands.open_embed_description_cancelled, user.id, buttonInteraction.user.id));
            embed.setColor(i.commands.embed_cancel_color as ColorResolvable);
            await interaction.editReply({
                embeds: [ embed ],
                components: [ ],
            })
            idleStatus();
        }
    )

    client.user.presence.set({ status: 'dnd', })

    debug('opening guild %s', interaction.guild.name);

    debug('got %d members', members.size);
    
    const targetAmount = members.size;
    let currentAmount = 0;

    const statusUpdater = setInterval(async () => {
        if(cancelled) return;
        const percentage = Math.round(currentAmount / targetAmount * 100);
        embed.setTitle(ii(i.commands.open_embed_title_progress, percentage));
        await interaction.editReply({
            embeds: [ embed ],
        });
    }, 5000)

    for(const member of members.values()) {
        if(cancelled) { break; }

        if (!member.roles.cache.has(openRole)) {
            await member.roles.add(openRole, i.commands.open_reason);
            await wait(1000);
        }
        currentAmount++;
    }

    clearInterval(statusUpdater)

    if(cancelled) { return; }

    // make sure there are no pending progress edits
    await wait(1000);

    const updateChannel = interaction.guild.channels.resolve(updateChannelId);
    if(updateChannel.isText()) {
        const messages = await updateChannel.messages.fetch({
            limit: 100,
        });
        let deleted = 0;
        for (const message of messages.values()) {
            if (message.author.id === client.user.id) {
                await message.delete();
                deleted++;
            }
        }
        debug('deleted %d messages from status channel', deleted);
        await updateChannel.send({
            embeds: [
                new MessageEmbed()
                    .setTitle(i.updates.open_embed_title)
                    .addFields(...(eventField ? [eventField] : []))
                    .setTimestamp()
                    .setColor(i.updates.open_embed_color as ColorResolvable),
            ]
        })
    }

    embed.setTitle(i.commands.open_embed_title_done)
    embed.setDescription(ii(i.commands.open_embed_description_done, user.id))
    embed.setColor(i.commands.open_embed_color as ColorResolvable)
    await interaction.editReply({
        embeds: [ embed ],
        components: [ ]
    });
    activeStatus();
}
import { SlashCommandBuilder } from '@discordjs/builders';
import { CacheType, ColorResolvable, CommandInteraction, MessageActionRow, MessageButton, MessageEmbed } from 'discord.js';
import { mainDebug, guilds, client, i18n as i } from '../main';
import { activeStatus, awaitButtonCancellation, idleStatus, ii } from '../utils';
import { promisify } from 'util';

const wait = promisify(setTimeout);
const debug = mainDebug.extend('close');

export const data = new SlashCommandBuilder()
    .setName(i.commands.close_command)
    .setDescription(i.commands.close_command_description)

export const perms = { access: true, lockdown: true, name: 'openclose' }
export async function execute(interaction: CommandInteraction<CacheType>) {
    const guildConfig = guilds[interaction.guild.id];
    const { updateChannel: updateChannelId, openRole } = guildConfig;
    const members = await interaction.guild.members.fetch()
    const isOpen = members
        .some(member => member.roles.cache.has(openRole));
    if(!isOpen) {
        await interaction.reply({
            content: i.commands.close_already_closed,
            ephemeral: true,
        });
        return;
    }

    const user = interaction.user;
    const embed = new MessageEmbed()
        .setTitle(i.commands.close_embed_title)
        .setDescription(ii(i.commands.close_embed_description, user.id))
        .setColor(i.commands.embed_progress_color as ColorResolvable)
        .setTimestamp()
    await interaction.reply({
        embeds: [ embed ],
        components: [
            new MessageActionRow()
                .addComponents(
                    new MessageButton()
                        .setCustomId(i.buttons.cancel.id)
                        .setLabel(i.buttons.cancel.label)
                        .setStyle('DANGER')
                ),
        ]
    });

    let cancelled = false;
    
    awaitButtonCancellation(
        await interaction.fetchReply(),
        interaction.channel,
        async (buttonInteraction) => {
            debug('close was cancelled by %s', buttonInteraction.user.tag);
            cancelled = true;

            embed.setTitle(i.commands.close_embed_title_cancelled_progress);
            embed.setDescription(i.commands.close_embed_description_cancelled_progress);
            embed.setColor(i.commands.embed_cancel_color as ColorResolvable);
            await interaction.editReply({
                embeds: [ embed ],
                components: [ ],
            })

            for(const member of members.values()) {
                if (!member.roles.cache.has(openRole)) {
                    await member.roles.add(openRole);
                    await wait(1000);
                }
            }

            embed.setTitle(i.commands.close_embed_title_cancelled);
            embed.setDescription(ii(i.commands.close_embed_description_cancelled, user.id, buttonInteraction.user.id));
            embed.setColor(i.commands.embed_cancel_color as ColorResolvable);
            await interaction.editReply({
                embeds: [ embed ],
                components: [ ],
            })
            activeStatus();
        }
    )

    client.user.presence.set({ status: 'dnd', })

    debug('closing guild %s', interaction.guild.name);

    debug('got %d members', members.size);

    const targetAmount = members.size;
    let currentAmount = 0;

    const statusUpdater = setInterval(async () => {
        if(cancelled) return;
        const percentage = Math.round(currentAmount / targetAmount * 100);
        embed.setTitle(ii(i.commands.close_embed_title_progress, percentage));
        await interaction.editReply({
            embeds: [ embed ],
        });
    }, 5000)

    for(const member of members.values()) {
        if(cancelled) { break; }

        if (member.roles.cache.has(openRole)) {
            await member.roles.remove(openRole, i.commands.close_reason);
            await wait(1000);
        }

        currentAmount++;
    }

    clearInterval(statusUpdater)

    if(cancelled) { return; }

    // make sure there are no pending progress edits
    await wait(1000);

    const updateChannel = interaction.guild.channels.resolve(updateChannelId);
    if(updateChannel.isText()) {
        const messages = await updateChannel.messages.fetch({
            limit: 100,
        });
        let deleted = 0;
        for (const message of messages.values()) {
            if (message.author.id === client.user.id) {
                await message.delete();
                deleted++;
            }
        }
        debug('deleted %d messages from status channel', deleted);
        await updateChannel.send({
            embeds: [
                new MessageEmbed()
                    .setTitle(i.updates.closed_embed_title)
                    .setTimestamp()
                    .setColor(i.commands.close_embed_color as ColorResolvable)
            ]
        })
    }

    embed.setTitle(i.commands.close_embed_title_done);
    embed.setDescription(ii(i.commands.close_embed_description_done, user.id));
    embed.setColor(i.commands.close_embed_color as ColorResolvable);
    await interaction.editReply({
        embeds: [ embed ],
        components: [ ]
    });
    idleStatus();
}
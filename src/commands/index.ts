import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v9';
import { client, clientId, guilds, token } from '../main';
import { mainDebug } from '../main';
import fs from 'fs';
import path from 'path';
import { Collection } from 'discord.js';

const debug = mainDebug.extend('commands');

client.commands = new Collection();

const commands = []
const commandFiles = fs
	.readdirSync(path.resolve(__dirname))
	.filter(file => 
		file.endsWith('.js')
		&& file !== path.basename(__filename)
	)
debug('command files: %o', commandFiles)

for (const file of commandFiles) {
	const command = require(`./${file}`)
	client.commands.set(command.data.name, command)
    commands.push(command.data.toJSON())
}

const rest = new REST({ version: '9' }).setToken(token);

for (const guildId in guilds) {
	rest.put(Routes.applicationGuildCommands(clientId, guildId), { body: commands })
		.then(() => debug('successfully registered application commands for %s.', guildId))
		.catch(console.error);
}
/**
 * Innostaja Discord bot.
 * By Johannes Vääräkangas
 */

import createDebug from 'debug'
import { Client, Collection, Intents } from 'discord.js'
import fs from 'fs';
import path from 'path';

const debug = createDebug('innostaja')
export { debug as mainDebug }
const {
  token,
  clientId,
  guilds: configGuilds,
  i18n,
}: typeof import('../config.template.json') = require(path.resolve(__dirname, '..', (process.env.DATA || '.'), 'config.json'))

const guilds: {
  [guildId: string]: {
    adminRole: string
    updateChannel: string
    openRole: string
  }
} = configGuilds;

export {
  token,
  clientId,
  guilds,
  i18n,
}
debug('token is "%s"', token)

export const client: Client<boolean> & {
  commands: Collection<string, any>
} = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MEMBERS,
  ],
}) as any;

import './commands'

const eventFiles = fs
  .readdirSync(path.resolve(__dirname, 'events'))
  .filter(file =>
    file.endsWith('.js')
  )
debug('event files: %o', eventFiles)

for (const file of eventFiles) {
  const event = require(path.resolve(__dirname, 'events', file))
  if (event.once) {
    client.once(event.name, event.execute)
  } else {
    client.on(event.name, event.execute)
  }
}

client.login(token)
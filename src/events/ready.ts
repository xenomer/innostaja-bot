import { client, guilds, mainDebug } from "../main"
import { activeStatus, idleStatus } from "../utils"

export const name = 'ready'
export async function execute() {
    mainDebug('loading open status...')
    let isOpen = false
    for (const guildId in guilds) {
        const guild = await client.guilds.fetch(guildId)
        const guildConfig = guilds[guildId]
        const members = await guild.members.fetch()
        const isGuildOpen = members.every(member => member.roles.cache.has(guildConfig.openRole))
        if (isGuildOpen) {
            isOpen = true;
            break;
        }
    }
    mainDebug('open status: %o', isOpen)
    if (isOpen) {
        activeStatus()
    } else {
        idleStatus()
    }
    mainDebug('bot ready')
}
import { CacheType, Interaction } from "discord.js"
import { client, mainDebug } from "../main"
import { handleAccess, setLockdown } from "../utils"

const debug = mainDebug.extend('interactions')
export const name = 'interactionCreate'
export async function execute(interaction: Interaction<CacheType>) {
    if (interaction.isCommand()) {
        const command = client.commands.get(interaction.commandName)
    
        if (!command) return;
    
        debug('command /%s by %s', interaction.commandName, interaction.user.tag)
    
        try {
            if (command.perms?.access && !await handleAccess(interaction, command.perms.name)) return;
            if (command.perms?.lockdown) setLockdown(command.perms.name, true)
            await command.execute(interaction)
            if (command.perms?.lockdown) setLockdown(command.perms.name, false)
        } catch (error) {
            console.error(error)
            await interaction.reply({
                content: 'Jees... :disguised_face: _\\*poistuu hiljaa ja nopeasti\\*_ (Joku taisi mennä pieleen)' +
                    '\n(_virhe: `' + error.message + '`_)',
                ephemeral: true,
            });
        }
    }
}

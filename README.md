# Innostaja Discord Bot
very cool (finnish isos discord bot made for Pornaisten Seurakunta at 4am lmao)

By Johannes Vääräkangas

Finnish.

very nice

## usage
just like `npm run dev:combo` (or build/run separately with
`build:watch` and `dev` tasks) or `npm run build` and then `npm start` to run in production.

## config
> also you can change data directory path (which is project directory by default) with
> `DATA` environment variable
```json
// config.json
{
	"token": "<TOKEN>",
    "clientId": "<CLIENT ID>",
    "i18n": {
        "commands": {
            "embed_progress_color": "#0000ee",
            "embed_cancel_color": "#777777",

            "open_command": "avaa",
            "open_command_description": "Avaa innostajat.",
            "open_command_event": "tapahtuma",
            "open_command_event_description": "Valinnainen tapahtuman nimi.",
            "open_already_open": "Innostajat on jo auki höhlö :confused:",
            "open_embed_title": ":hourglass_flowing_sand: Innostajia avataan...",
            "open_embed_title_progress": ":hourglass_flowing_sand: Innostajia avataan... (%d%)",
            "open_embed_title_done": "Innostajat avattiin",
            "open_embed_title_cancelled": "Innostajien avaaminen peruutettiin :sos:",
            "open_embed_title_cancelled_progress": "Peruutetaan innostajien avaamista :ambulance:",
            "open_embed_description": "<@%s> aloitti innostajien avauksen",
            "open_embed_description_done": "<@%s> avasi innostajat",
            "open_embed_description_cancelled": "<@%s> yritti avata innostajat, mutta <@%s> peruutti sen.",
            "open_embed_description_cancelled_progress": ":ambulance::ambulance::ambulance::ambulance::ambulance:",
            "open_embed_color": "#00cc00",
            "open_embed_event_name": ":calendar_spiral: Tapahtuma",
            "open_embed_event_value": "_%s_",
            
            
            "close_command": "sulje",
            "close_command_description": "Sulje innostajat.",
            "close_already_closed": "Innostajat on jo kiinni kälyääpiö :confused:",
            "close_embed_title": ":hourglass_flowing_sand: Innostajia suljetaan...",
            "close_embed_title_progress": ":hourglass_flowing_sand: Innostajia suljetaan... (%d%)",
            "close_embed_title_done": "Innostajat suljettiin",
            "close_embed_title_cancelled": "Innostajien sulkeminen peruutettiin :sos:",
            "close_embed_title_cancelled_progress": "Peruutetaan innostajien sulkemista :ambulance:",
            "close_embed_description": "<@%s> aloitti innostajien sulkemisen",
            "close_embed_description_done": "<@%s> sulki innostajat",
            "close_embed_description_cancelled": "<@%s> yritti sulkea innostajat, mutta <@%s> peruutti sen.",
            "close_embed_description_cancelled_progress": ":ambulance::ambulance::ambulance::ambulance::ambulance:",
            "close_embed_color": "#cc0000",
            
            "open_reason": "Innostajien avaus",
            "close_reason": "Innostajien sulkeminen"
        },
        "buttons": {
            "cancel": {
                "id": "cancel",
                "label": "Peruuta"
            }
        },
        "updates": {
            "open_embed_title": ":unlock: Innostajat ovat auki! :unlock:",
            "open_embed_color": "#00cc00",
            "closed_embed_title": ":lock: Innostajat ovat kiinni! :frowning: :lock:",
            "closed_embed_color": "#cc0000"
        }
    },
    "guilds": {
        "<GUILD ID>": {
            "updateChannel": "<CHANNEL ID>",
            "openRole": "<ROLE ID>",
            "adminRole": "<ROLE ID>"
        }
    }
}
```

## todos

- tests